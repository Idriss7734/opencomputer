local com = require('component')
local gpu = com.gpu
local term = require("term")
local event = require("event")
local w,h = gpu.getResolution()
local std = require 'std'
local computer = require("computer")
keyboard = require("keyboard")


event.shouldInterrupt = function()
   return false
end

local nOption = 1

local function drawMenu()
   term.clear()
   term.setCursor(1,1)
   term.write("GM-NAA I/O ")
   term.setCursor(1,2)
   print(os.date("%Y/%m/%d"))
end

local function printFolder(directoryList, size) 
   local index = 1
   while index ~= size + 1 do
      std.print(math.floor(h/2) + index, ((nOption == index) and "[" .. directoryList[index] .. "]") or directoryList[index])
      index = index + 1
   end
end

--GUI

local function drawFrontend(list, size)
   std.print( math.floor(h/2) - 3, "")
   std.print( math.floor(h/2) - 3, "")
   std.print( math.floor(h/2) - 3, "")
   std.print( math.floor(h/2) - 3, "")
   std.print( math.floor(h/2) - 2, "Start Menu" )
   std.print( math.floor(h/2) - 1, "")

   printFolder(list, size)
   
   std.print( math.floor(h/2) + 4, "")


   term.setCursor(w-11,1) 
   term.write(list[nOption])
end

local function choice(list)
   if list[nOption] == "Back" then
      os.execute("back")
   else
      os.execute("/home/programs/" .. list[nOption] .. "/start.lua")
   end
end

--Display
drawMenu()
local list = std.scandir("/home/programs")
local size = std.size(list)
table.insert(list, "back")
size = size + 1
local index = 1
while index ~= size + 1 do
   list[index] = std.firstToUpper(list[index])
   index = index + 1
end

drawFrontend(list, size)

while true do
   local a,b,key  = event.pull()
   os.sleep(0.1)
   if key == 119 then
      if nOption > 1 then
         nOption = nOption - 1
         drawMenu()
         drawFrontend(list, size)   
      end
   elseif key == 115 then
      if nOption < size then
         nOption = nOption + 1
         drawMenu()
         drawFrontend(list, size)
      end
   elseif key == 13 then
      break
   end
end

term.clear()
--Conditions

choice(list)
