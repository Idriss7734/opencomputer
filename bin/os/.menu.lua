local com = require('component')
local gpu = com.gpu
local term = require("term")
local event = require("event")
local w,h = gpu.getResolution()
local std = require 'std'
local computer = require("computer")
keyboard = require("keyboard")

event.shouldInterrupt = function()
   return false
end

local nOption = 1

local function drawMenu()
   term.clear()
   term.setCursor(1,1)
   term.write("GM-NAA I/O ")
   term.setCursor(1,2)
   print(os.date("%Y/%m/%d"))

   term.setCursor(w-11,1)
if nOption == 1 then
   term.write("Command")
elseif nOption == 2 then
   term.write("Programs")
elseif nOption == 3 then
   term.write("Programs")
elseif nOption == 4 then
   term.write("Update")
elseif nOption == 5 then
   term.write("Shutdown")
else
end

end

--GUI
term.clear()
local function drawFrontend()
   local centerX = math.floor(w / 2)
   local centerY = math.floor(h / 2)

   -- Draw a background rectangle
   std.print(centerX - 10, centerY - 5, string.rep(" ", 20))

   -- Draw the title
   std.print(centerX - 5, centerY - 3, "GM-NAA I/O")

   -- Draw the menu options
   std.print(centerX - 5, centerY - 1, ((nOption == 1) and "[ Command ]") or "Command")
   std.print(centerX - 5, centerY + 0, ((nOption == 2) and "[Files]") or "Files")
   std.print(centerX - 5, centerY + 1, ((nOption == 3) and "[Programs]") or "Programs")
   std.print(centerX - 5, centerY + 2, ((nOption == 4) and "[Update]") or "Update")
   std.print(centerX - 5, centerY + 3, ((nOption == 5) and "[Shutdown]") or "Shutdown")
end

--Display
drawMenu()
drawFrontend()
event.push("0")
while true do
   local a,b,key  = event.pull()
   os.sleep(0.1)
   if key == 119 then
      if nOption > 1 then
         nOption = nOption - 1
         drawMenu()
         drawFrontend()   
      end
   elseif key == 115 then
      if nOption < 4 then
         nOption = nOption + 1
         drawMenu()
         drawFrontend()
      end
   elseif key == 13 then
      break
   end
end

term.clear()
--Conditions

if nOption  == 1 then
   os.execute("/bin/os/.command")
if nOption  == 2 then
   os.execute("/bin/os/.files")
elseif nOption == 3 then
   os.execute("/bin/os/.programs")
elseif nOption == 4 then
   os.execute("/bin/os/.update")
elseif nOption == 5 then
   computer.shutdown(false)
else
   os.execute("/bin/os/.UninstallDialog")
end


