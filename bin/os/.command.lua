local term = require("term")

term.clear()
term.setCursor(1,1)
print("Admin shell")
print("To exit, type 'back'")
print("--------------------")
local s = ""
while s ~= "back" do
    s = io.read("*l") -- read a line (default when no parameter is given) 
    os.execute(s)
end
