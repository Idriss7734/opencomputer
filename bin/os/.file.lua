-- This display a list of all files in the files folder and allows the user to select one
-- of them to open it in the editor.   

local fs = require("filesystem")
local shell = require("shell")
local term = require("term")
local event = require("event")
local component = require("component")

local args, options = shell.parse(...)

if args[1] == "help" then
    print("Usage: file")
    return
end

local files = fs.list("files")

if #files == 0 then
    print("No files found")
    return
end

local selected = 1
local max = #files

while true do
    term.clear()

    for i = 1, max do
        if i == selected then
            term.setCursor(1, i)
            print(files[i])
        else
            term.setCursor(1, i)
            print(files[i])
        end
    end

    local _, _, x, y = event.pull("touch")

    if x == 1 then
        selected = selected - 1

        if selected < 1 then
            selected = max
        end

    elseif x == 2 then
        selected = selected + 1

        if selected > max then
            selected = 1
        end

    elseif x == 3 then
        if fs.exists("files/" .. files[selected]) then
            shell.execute("edit files/" .. files[selected])
        end
        return

    elseif x == 4 then
        return

    end

end
