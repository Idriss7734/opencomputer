local auto_progress = require 'auto_progress'
local term = require("term")
local thread = require "thread"

term.clear()
term.setCursor(1,1)
print("GM-NAA I/O")
print("Loading...")
term.setCursor(1,3)
os.sleep(1)
local loading = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
local pbar = auto_progress.new(loading)
pbar.totalWork = 11
for _, duration in pairs(loading) do
    --simulate an action
    os.sleep(0.1)
    --update progress bar: 1 step done
    pbar.update(1)
    pbar.draw()
  end
--tell progress bar that the action has been finished
pbar.finish()

os.execute("bin/os/.menu")