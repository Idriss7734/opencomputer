-- Import the mining library
local mining = require("mining")
local robotFrame = require("robotFrame")

-- Define the coordinates of the charging station
local chargingStation = {
  x = 0,
  y = 0,
  z = 0
}
-- Define de coordinates of the mining area
local miningArea = {
  x = 0,
  y = 0,
  z = 0,
  width = 0,
  height = 0,
  depth = 0
}

-- Define the coordinates of the fuel chest location (below the robot when charging)
local fuelChestLocation = {
  x = 0,
  y = 0,
  z = -1
}

-- Define the tool chest location
local toolChestLocation = {
  x = 0,
  y = 0,
  z = 1
}
-- Define storage output
local storageOutput = {
  x = 0,
  y = -1,
  z = 0
}

-- Function to move the robot to the charging station
function moveToChargingStation()
  robotFrame.moveTo(chargingStation.x, chargingStation.y, chargingStation.z)
end

-- Function to check if the robot is in the mining area
function robotFrame.inArea(width, height, depth)
  return miningArea.x - width <= x and x <= miningArea.x + width and
    miningArea.y - height <= y and y <= miningArea.y + height and
    miningArea.z - depth <= z and z <= miningArea.z + depth
end

-- Function to get user input for zone to mine
function getZoneToMine()
  while true do
    print("Enter the zone to mine (e.g. 3x3x3):")
    local input = io.read()
    local x, y, z, width, height, depth = input:match("(%d+)x(%d+)x(%d+)x(%d+)x(%d+)x(%d+)")

    if width and height and depth then
      return tonumber(x), tonumber(y), tonumber(z), tonumber(width), tonumber(height), tonumber(depth)
    else
      print("Invalid input. Please try again.")
    end
  end
end

-- Function to empty inventory into storage
function emptyInventory()
  for i=1,14 do
    robot.select(i)
    robot.dropUp()
  end

-- Function to get new tools if needed
function getNewTools()
  -- Get new tools
  if robotFrame.needsNewTool() == 0 then
    robot.select(16)
    robot.suckDown()
  end
end

-- Function to get new fuel if needed
function getNewFuel()
  if robot.count(15) <= 10 then
    robot.select(15)
    robot.suckDown()
  end
end

-- Main program
function main()
  -- Get the zone to mine from the user
  local miningArea.x, miningArea.y, miningArea.z, miningArea.width, miningArea.height, miningArea.depth = getZoneToMine()

  -- loop that mines the zone
  while true do
    -- condition that check if the robot should drop off
    if robotFrame.shouldDropOff() then
      -- drop off and return to continue mining
      robotFrame.dropOff()
      robotFrame.moveTo(chargingStation.x, chargingStation.y, chargingStation.z)
      robotFrame.turnTowards(0)
      -- wait to be full charge
      while not robotFrame.isEnergyLow() do
        os.sleep(5)
      end
    end

    -- condition that check if the robot is in the mining area
    if not robotFrame.inArea(miningArea.x, miningArea.y, miningArea.z) then
      print("Not in mining area!")
      -- Move to the mining area
      robotFrame.moveTo(miningArea.x, miningArea.y, miningArea.z)
      break
    end

    -- mine the zone and get updated zone to mine
    miningArea.x, miningArea.y, miningArea.z = mining.mineRect(miningArea.x, miningArea.y, miningArea.z, miningArea.width, miningArea.height, miningArea.depth)
  end
end