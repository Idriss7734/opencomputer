local component = require("component")
local thread = require("thread")
local robotFrame = require 'robotFrame'
local shell = require("shell")

local x,y,z = 0, 0, 0
local miningDown = true --from which direction are we mining the column
local args = {0,0,0}
local args, options = shell.parse(...)

if args[1] == "help" or not component.isAvailable("robot") or not component.isAvailable("inventory_controller") then
  io.write("\n" .. 
"NAME\n" .. 
"  quarry_mainframe - Dig set space with robot fleet\n" .. 
"       \n" .. 
"REQUIREMENTS\n" .. 
"  Must run on a robot with Inventory controller \n" .. 
"  upgrade. Optional Chunkloader upgrade is\n" .. 
"  recommended.\n" .. 
"\n" .. 
"SYNOPSIS\n" .. 
"  quarry_mainframe [OPTIONS]...\n" .. 
"    Starts mining.\n" .. 
"         \n" .. 
"  quarry_mainframe help\n" .. 
"    Displays this help\n" .. 
"\n" .. 
"OPTIONS\n" .. 
"  -c, --continue\n" .. 
"    Resumes the quarry progress on the last mined\n" .. 
"    column. Useful for example if the robot got\n" .. 
"    stuck and you had to break it. Just place the\n" .. 
"    robot at the starting point, use this option\n" .. 
"    and it will continue where it was.\n" .. 
"\n" .. 
"  --continue=n\n" .. 
"    Where *n* is the side of the already mined\n" .. 
"    area. The robot will continue mining roughly\n" .. 
"    at the end of the entered area. Useful if\n" .. 
"    replacing a lost robot. If you want to replace\n" .. 
"     a robot you did not lose, it is better copy\n" .. 
"     the state file (see *State saving*)\n" .. 
"\n" .. 
"  -a, --alwaysChangeTool\n" .. 
"    The robot will change its primary tool\n" .. 
"    (pickaxe) every time it visits the start\n" .. 
"    point, not only when it is almost broken.\n" .. 
"    Recommended when using a repairable tool like\n" .. 
"    in the example setups.\n" .. 
"           \n" .. 
"HELP\n" .. 
"  Detailed guide on using this program can be\n" .. 
"  found on\n" .. 
"  https://gitlab.com/Idriss7734/home/programs/quarry_mainframe\n" .. 
"  ")
  
  if not component.isAvailable("robot") or not component.isAvailable("inventory_controller") then
    io.stderr:write("can only run on robots with an inventory controller upgrade\n")
  end
  return
elseif args[1] ~= nil and args[2] ~= nil and args[3] ~= nil then
  x = args[1]
  y = args[2]
  z = args[3]
end

local function mineLoop(noThread, numBlock)
  print("Starting MineLoop")
  print("Dropping off")
  robotFrame.dropOff()
  miningDown = true
  print("Enterring Loop")
  while true do
    local X, Y, Z = numBlock[noThread][1], numBlock[noThread][2], numBlock[noThread][3]
    print("Moving to")
    robotFrame.moveTo(X, Y, Z)
    print("Mining")
    robotFrame.mine(miningDown)
    
    miningDown = not miningDown

    --save state
    file=io.open("quarry_state","w")
    file:write(column)
    file:close()
  end
end

local function robotNeed()
  local spaceToMine = x * y * z 
  local robotNumberNeed = 1
  local registerRobot = 1
  -- load register robot
  file=io.open("register_robot","r")

  if file then
    contents=tonumber(file:read())
    registerRobot = contents or 0
    file:close()
  end
  
  -- Get the number of robot that are need
  while spaceToMine % robotNumberNeed  ~= 0 do
    robotNumberNeed = robotNumberNeed + 1
  end
  if (robotNumberNeed > registerRobot) then
    robotNumberNeed = registerRobot
  end

  -- allocate space to mine for each robot
  local blockPRobot = spaceToMine / robotNumberNeed
  local rows, cols = robotNumberNeed, 3 
  local space = {}          -- create the matrix
  local mX, mY, mZ = x / robotNumberNeed, y / robotNumberNeed, z / robotNumberNeed 

  for i = 1, rows do
    space[i] = {}     -- create a new row
    for j = 1, cols do
      space[i][1] = mX * i
      space[i][2] = mY * i
      space[i][3] = mZ * i
    end
  end


  return robotNumberNeed, space
end

if options.alwaysChangeTool or options.a then
  robotFrame.alwaysChangeTool = true
end

if type(options.continue) == "string" then
  column = ( (math.floor((tonumber(options.continue) / 5)) - 1)^2)*5
else
  if options.continue or options.c then
    file=io.open("quarry_state","r")
    if file then
      contents=tonumber(file:read())
      column = contents or 0
      file:close()
    end
  end
end

local robotNumber, blockPRobot = robotNeed()
local noThread = 1
local t = {}

while noThread <= robotNumber do
  table.insert(t, thread.create(function(noThread, numBlock)
    mineLoop(noThread, numBlock)
    end, noThread, blockPRobot))

    noThread = noThread + 1
end
thread.waitForAll(t)