-- Initialisation des variables
local component = require("component")
local computer = require("computer")
local event = require("event")
local os = require("os")
local com = require('component')
local gpu = com.gpu
local term = require("term")

local std = {} -- Table qui stocke les fonctions standard

-- Fonction pour afficher un message centré à l'écran
-- y:curPose ; s:string to center
function std.print(y, s)
    assert(y ~= nil and type(y) == "number", "y has to be nil or a number!")
    assert(s ~= nil and type(s) == "string", "s has to be nil or a string!")

    local w, h = gpu.getResolution()

    local x = math.floor((w - string.len(s)) / 2)
    term.setCursor(x, y)
    term.write(s)
end

-- Lua implementation of table size() function
function std.size(T)
    local count = 0
    for _ in pairs(T) do
        count = count + 1
    end
    return count
end

-- Lua implementation of PHP scandir function
function std.scandir(directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls -a "' .. directory .. '"')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

-- Lua implementation of upper function. Only first letter
function std.firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end

-- Lua implementation of lower function. Only first letter
function std.firstToLower(str)
    return (str:gsub("^%l", string.lower))
end

-- Lua implementation of upper function
function std.ToUpper(str)
    return (str.upper())
end

-- Lua implementation of lower function
function std.ToLower(str)
    return (str.lower())
end

-- Fonction pour attendre un certain nombre de secondes
function std.sleep(seconds)
    os.sleep(seconds)
end

-- Fonction pour vérifier la mémoire disponible sur l'ordinateur
function std.freeMemory()
    return computer.freeMemory()
end

-- Fonction pour vérifier l'espace de stockage disponible sur le disque dur
function std.freeSpace(path)
    return component.proxy(component.list("filesystem")()).spaceTotal() -
        component.proxy(component.list("filesystem")()).spaceUsed()
end

-- Fonction pour attendre jusqu'à ce qu'un événement spécifique se produise
function std.waitForEvent(eventType, timeout)
    local timer = os.startTimer(timeout)
    while true do
        local e = table.pack(event.pull(timeout))
        if e[1] == eventType then
            return table.unpack(e, 2)
        elseif e[1] == "timer" and e[2] == timer then
            return nil
        end
    end
end

-- Fonction pour afficher un message d'erreur et quitter le programme
function std.error(message)
    io.stderr:write(message .. "\n")
    os.exit(1)
end

return std
