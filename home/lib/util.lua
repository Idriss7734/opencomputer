--Initialisation des variables
local component = require("component")
local robot = component.robot
local inventory = component.inventory_controller

--Fonction pour ramasser tous les items d'un inventaire
function pickUpAllItems()
  local slots = robot.inventorySize()
  for slot = 1, slots do
    inventory.suckFromSlot(0, slot)
  end
end

--Fonction pour déposer tous les items de l'inventaire dans un coffre
function depositAllItems()
  local slots = robot.inventorySize()
  for slot = 1, slots do
    inventory.dropIntoSlot(0, slot)
  end
end

--Fonction pour s'approvisionner en items à partir d'un coffre
function restock()
  local slots = robot.inventorySize()
  for slot = 1, slots do
    local stack = inventory.getStackInInternalSlot(slot)
    if not stack then --Si l'emplacement est vide, chercher un item dans le coffre
      for chestSlot = 1, 27 do
        local chestStack = inventory.getStackInSlot(0, chestSlot)
        if chestStack and robot.compareTo(chestSlot) then --Si l'item est le même, le prendre
          inventory.suckFromSlot(0, chestSlot, stack.size)
        end
      end
    end
  end
end

--Fonction pour vérifier si un emplacement de l'inventaire contient un item spécifique
function hasItem(itemName)
  local slots = robot.inventorySize()
  for slot = 1, slots do
    local stack = inventory.getStackInInternalSlot(slot)
    if stack and stack.name == itemName then
      return true
    end
  end
  return false
end
