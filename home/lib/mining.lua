-- home/lib/mining.lua

-- Import the robotFrame library
local robotFrame = require("robotFrame")

-- Import the component library
local component = require("component")
local robot = component.robot
local inventory = component.inventory_controller

-- Function to mine a block
function mineBlock()
  robot.select(1) --Sélectionne le premier emplacement de l'inventaire
  local success, block = robot.detect()
  if success then --Si un bloc est détecté, le mine
    robot.swing()
    while inventory.getStackInInternalSlot(1) do --Attend que l'emplacement de l'inventaire soit vide
      os.sleep(0.1)
    end
    inventory.suckFromSlot(0, 1) --Ramasse le bloc dans l'inventaire
end
  

-- Function to mine a line of blocks
function mineLine(length)
  for i = 1, length do
    mineBlock()
    robot.move(1)
  end
  robot.move(-1*length, 0, 0) -- Return to the start of the line
end

-- Function to mine a rectangle of blocks
function mineRect(x, y, z, width, height, depth, )
  -- Loop through the specified rectangle
  Uwidth = width
  Uheight = height
  Udepth = depth
  zoneToMine = {Uwidth, Uheight, Udepth}
  for x = -width, width do
    for y = -height, height do
      for z = -depth, depth do
        mineBlock()
        
        -- Update the zoneToMine table
        if x == zoneToMine[1] then
          zoneToMine[1] = zoneToMine[1] - 1
        elseif x == -zoneToMine[1] then
          zoneToMine[1] = zoneToMine[1] - 1
        end
        
        if y == zoneToMine[2] then
          zoneToMine[2] = zoneToMine[2] - 1
        elseif y == -zoneToMine[2] then
          zoneToMine[2] = zoneToMine[2] - 1
        end
        
        if z == zoneToMine[3] then
          zoneToMine[3] = zoneToMine[3] - 1
        elseif z == -zoneToMine[3] then
          zoneToMine[3] = zoneToMine[3] - 1
        end
        
        -- Update the zone to mine (width, height, depth)
        zoneToMine = {width, height, depth}
        -- Check if the robot should pause
        if robotFrame.shouldDropOff() then
          -- Return to the main loop to allow charging
          return zoneToMine
        end
      end
    end
  end

end