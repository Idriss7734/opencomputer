-----------------------------------------------------
--name       : lib/robot.lua
--description: Contains various functions for programming robots
--author     : Idriss7734
--github page: https://github.com/Idriss7734/opencomputer
-----------------------------------------------------

local component = require("component")
local computer = require("computer")
local robot = require("robot")
local shell = require("shell")
local sides = require("sides")
local math = require("math")
local inventory = component.inventory_controller

--CONSTANTS--
local NEEDS_CHARGE_THRESHOLD = 0.2
local FULL_CHARGE_THRESHOLD = 0.95
local TOOL_MIN_DURABILITY = 0.1
local MIN_ENERGY = 0.3
local alwaysChangeTool = false

local args, options = shell.parse(...)

--THE LIBRARY--
robotFrame = {}

local additionalTools = 0

local r = component.robot
local x, y, z, f = 0, 0, 0, 0
local dropping = false -- avoid recursing into drop()
local delta = {[0] = function() x = x + 1 end, [1] = function() y = y + 1 end,
               [2] = function() x = x - 1 end, [3] = function() y = y - 1 end} -- records move in certain direction

function robotFrame.turnRight()
  robot.turnRight()
  f = (f + 1) % 4
end

function robotFrame.turnLeft()
  robot.turnLeft()
  f = (f - 1) % 4
end

function robotFrame.turnTowards(side)
  if f == side - 1 then
    turnRight()
  else
    while f ~= side do
      turnLeft()
    end
  end
end

function robotFrame.swingWithAllTools(side) --swing using all the different additional tools
  local result, reason = r.swing(side)
  for i = 0, additionalTools - 1 do
    local slot = r.inventorySize() - i
    r.select(slot)
    inventory.equip()
    result, reason = r.swing(side)
    inventory.equip()
    if result then
      r.select(1)
      return result
    end
  end
  r.select(1)
  return false
end

function robotFrame.clearBlock(side, cannotRetry) --return false if fails and cannot retry
  while r.suck(side) do end
  local result, reason = r.swing(side)
  if not result then
    local _, what = r.detect(side)
    
    if cannotRetry and what ~= "air" and what ~= "entity" then
      return false
    end
    if reason == "block" then
      return swingWithAllTools(side)
    end
  end
  return true
end

function robotFrame.tryMove(side, digFirst) --returns false if fails (10 tries on removing obstacle failed)
  side = side or sides.forward
  local tries = 10
  if digFirst then
    robot.clearBlock(side)
  end
  while not r.move(side) do
    tries = tries - 1
    if not robotFrame.clearBlock(side, tries < 1) then
      io.write("could not move in direction " .. side .. " on x: " .. x .. " y: " .. y .." z(up/down): " .. z .. "\n")
      return false
    end
  end
  if side == sides.down then
    z = z + 1
  elseif side == sides.up then
    z = z - 1
  else
    delta[f]()
  end
  return true
end

function robotFrame.moveTo(tx, ty, tz, backwards)
  local axes = {
    function()
      while z > tz do
        robotFrame.tryMove(sides.up)
      end
      while z < tz do
        robotFrame.tryMove(sides.down)
      end
    end,
    function()
      if y > ty then
        robotFrame.turnTowards(3)
        repeat robotFrame.tryMove() until y == ty
      elseif y < ty then
        robotFrame.turnTowards(1)
        repeat robotFrame.tryMove() until y == ty
      end
    end,
    function()
      if x > tx then
        robotFrame.turnTowards(2)
        repeat robotFrame.tryMove() until x == tx
      elseif x < tx then
        robotFrame.turnTowards(0)
        repeat robotFrame.tryMove() until x == tx
      end
    end
  }
  if backwards then
    for axis = 3, 1, -1 do
      axes[axis]()
    end
  else
    for axis = 1, 3 do
      axes[axis]()
    end
  end
end

function robotFrame.needsNewTool()
  local ret = not robot.durability() or robot.durability() < TOOL_MIN_DURABILITY
  if (ret) then
    io.write("Tool duarbility is getting low: " .. (robot.durability() or "nil") .." < " .. TOOL_MIN_DURABILITY .. "\n")
  end
  return ret

end

function robotFrame.isInventoryFull()
  local inventorySize = robot.inventorySize()
  local ret = robot.count(inventorySize - 1 - additionalTools) ~= 0 --rather sooner 
  if ret then
    io.write("inventory is almost full" .. "\n")
  end
  return ret
end

function robotFrame.isEnergyLow()
  local ret = computer.energy() / computer.maxEnergy() < MIN_ENERGY
  if (ret) then
    io.write("energy is low: " .. computer.energy() / computer.maxEnergy() .." < " .. MIN_ENERGY .. "\n")
  end
  return ret
end

function robotFrame.needsNewAdditionalTool()
  for i = 0, additionalTools - 1 do
    local slot = r.inventorySize() - i
    local itemStack = inventory.getStackInInternalSlot(slot)
    if itemStack then
      local dmg = itemStack.damage
      local maxdmg = itemStack.maxDamage
      local durability = 1 - dmg/maxdmg
      if durability < TOOL_MIN_DURABILITY or maxdmg - dmg < 3 then
        return true
      end
    else
      return true
    end
  end
  return false
end

function robotFrame.shouldDropOff() -- returns true if the robot should dropOff
  return not dropping and (robotFrame.needsNewTool() or robotFrame.isEnergyLow() or robotFrame.isInventoryFull() or robotFrame.needsNewAdditionalTool())
end

function robotFrame.dropOff() -- drops off and stay on start
  while (robotFrame.shouldDropOff()) do
    io.write("returning to drop off..." .. "\n")
    dropping = true

    robotFrame.moveTo(0, 0, 0)
    robotFrame.turnTowards(2)

    local inventorySize = robot.inventorySize()

    for slot = 1, inventorySize - additionalTools do
      if robot.count(slot) > 0 then
        robot.select(slot)
        local wait = 1
        repeat
          if not robot.drop() then
            os.sleep(wait)
            wait = math.min(10, wait + 1)
          end
        until robot.count(slot) == 0
      end
    end
    robot.select(1)

    dropping = false

    --wait for recharge
    while computer.energy() / computer.maxEnergy() < 0.9 do
      os.sleep(5)
    end

    --change tool
    if (robot.durability() ~= 1 and alwaysChangeTool) or needsNewTool() then
      moveTo(0,1,0)
      turnTowards(2)
      robot.select(1)
      robot.suck()
      inventory.equip()
      moveTo(0,1,-1)
      turnTowards(2)
      robot.drop()
    end

    --change additional tools
    --drop almost broken ones
    for i = 0, additionalTools - 1 do
      local slot = inventorySize - i
      local itemStack = inventory.getStackInInternalSlot(slot)
      if itemStack then
        local dmg = itemStack.damage
        local maxdmg = itemStack.maxDamage
        local durability = 1 - dmg/maxdmg
        if durability < TOOL_MIN_DURABILITY or maxdmg - dmg < 3 then
          robotFrame.moveTo(0,2 + i,-1)
          robotFrame.turnTowards(2)
          robot.select(slot)
          robot.drop()
        end
      end
    end
    --take new ones
    additionalTools = 0
    while true do
      local slot = inventorySize - additionalTools
      if robot.count(slot) == 0 then
        robotFrame.moveTo(0,2 + additionalTools,0)
        robotFrame.turnTowards(2)
        robot.select(slot)
        robot.suck()
        if robot.count(slot) == 0 then
          break
        end
      end
      additionalTools = additionalTools + 1
    end

    robot.select(1)
    robotFrame.moveTo(0, 0, 0)
    robotFrame.turnTowards(0)
  end
end

-- if side == nil, the robot won't move
function robotFrame.mineStep(side)
  robotFrame.clearBlock(sides.forward)

  if side.forward == nil then
    return true
  end

  return robotFrame.tryMove(side, true)
end

function robotFrame.mine(mineDown, startZ, location) -- drops off and stay on start
  if mineDown then
    while location[3] ~= startZ and robotFrame.mineStep(sides.down) do
      if robotFrame.shouldDropOff() then
        --drop off and return to continue mining
        local rx, ry, rz = x, y, z
        robotFrame.dropOff()
        robotFrame.moveTo(rx, ry, rz, true)
        robotFrame.mine(true, startZ, location)
        return
      end
    end
    if z == startZ then
      while robotFrame.tryMove(sides.down) do end
    end
    if robotFrame.traverseZ == -1 then
      robotFrame.traverseZ = z - 4 -- to avoid bedrock
    end
    return robotFrame.moveTo(x,y, robot.traverseZ)
  else
    --mining from down up
    local sz = z
    while robotFrame.mineStep(sides.down) do end
    if robotFrame.traverseZ == -1 then
      robotFrame.traverseZ = z - 4
    end
    robotFrame.moveTo(x,y,sz - 1)
    while z > 0 do
      if not robotFrame.mineStep(sides.up) then
        return false
      end
      if robotFrame.shouldDropOff() then
        --return to start using last column, then drop off and continue mining this column, but from up.
        local rx, ry, rz = x, y, z
        --we use the previous column to go up
        local lx, ly = location[1], location[2]
        robotFrame.moveTo(lx, ly, z, true)
        robotFrame.dropOff()
        robotFrame.moveTo(rx, ry, 0, true)
        robotFrame.miningDown = true
        --also when minig down, skip the part from botton that is already mined mined 
        robotFrame.mine(true, rz + 1)
        return
      end
    end
    robotFrame.mineStep(nil)
    return true
  end
end

return robotFrame