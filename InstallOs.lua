local computer = require("computer")

print("Downloading GM-NAA I/O")
print("Downloading autorun")

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/autorun.lua")  then
    os.execute("mv /home/autorun.lua /autorun.lua")
end

print("Downloading /bin/back.lua")

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/bin/back.lua") then 
    os.execute("mv /home/back.lua /bin/back.lua")
end

print("Downloading /bin/os")

os.execute("mkdir /bin/os")

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/bin/os/.command.lua") then
    os.execute("mv /home/.command.lua /bin/os/.command.lua")
end

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/bin/os/.menu.lua") then
    os.execute("mv /home/.menu.lua /bin/os/.menu.lua")
end

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/bin/os/.programs.lua") then
    os.execute("mv /home/.programs.lua /bin/os/.programs.lua")
end

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/bin/os/.update.lua")  then
    os.execute("mv /home/.update.lua /bin/os/.update.lua")
end

print("Downloading /home/lib")

os.execute("mkdir /home/lib")

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/lib/auto_progress.lua") then
    os.execute("mv /home/auto_progress.lua /home/lib/auto_progress.lua")
end

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/lib/std.lua") then
    os.execute("mv /home/std.lua /home/lib/std.lua")
end

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/lib/robotFrame.lua") then
    os.execute("mv /home/robotFrame.lua /home/lib/robotFrame.lua")
end

print("Downloading /home/programs")
print("Downloading /home/programs/quarry/quarry.lua")

os.execute("mkdir /home/programs")

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/programs/quarry/start.lua") then
    os.execute("mkdir /home/programs/quarry")
    os.execute("mv /home/start.lua /home/programs/quarry/start.lua")
end

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/programs/quarryMainframe/start_robot.lua") then
    os.execute("mkdir /home/programs/quarryMainframe")
    os.execute("mv /home/start.lua /home/programs/quarryMainframe/start.lua")
end

if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/programs/quarryMainframe/start.lua") then
    os.execute("mkdir /home/programs/quarryMainframe")
    os.execute("mv /home/start.lua /home/programs/quarryMainframe/start.lua")
end
print("Downloading /home/programs/pong/pong.lua")


if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/programs/pong/pong.lua") then
    os.execute("mkdir /home/programs/pong")
    os.execute("mv /home/pong.lua /home/programs/pong/pong.lua")
    if os.execute("wget -f https://gitlab.com/Idriss7734/opencomputer/-/raw/main/home/programs/pong/start.lua") then
        os.execute("mv /home/start.lua /home/programs/pong/start.lua")
    end
end

print("Rebooting in:")
print("3")
os.sleep(1)
print("2")
os.sleep(1)
print("1")
os.sleep(1)

computer.shutdown(true)